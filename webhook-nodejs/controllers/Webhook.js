'use strict';

var url = require('url');


var Webhook = require('./WebhookService');


module.exports.webhook = function webhook (req, res, next) {
  Webhook.webhook(req.swagger.params, res, next);
};
